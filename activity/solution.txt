Make a new file called solution.txt inside
s01-a1 folder and answer the following
questions based on the image:

1. List the books Authored by Marjorie Green.

A:
- BU1032: The Busy Executive's Database Guide 
- BU2075: You Can Combat Computer Stress!

2. List the books Authored by Michael O'Leary.

A:
- BU1111: Cooking with Computers
- "title_id": "TC7777"

3. Write the author/s of "The Busy Executive’s Database Guide".

A:
- 213-46-8915: Marjorie Green
- 409-56-7008: Abraham Bennet

4. Identify the publisher of "But Is It User Friendly?".

A:
- 1389: Algodata Infosystems

5. List the books published by Algodata Infosystems.

A:
- BU1032: The Busy Executive's Database Guide
- BU1111: Cooking with Computers
- BU7832: Straight Talk About Computers
- PC1035: But Is It User Friendly?
- PC8888: Secrets of Silicon Valley
- PC9999: Net Etiquette